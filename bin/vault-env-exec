#!/usr/bin/env python3

import json
import os
import sys
import time
from json import JSONDecodeError
from tempfile import NamedTemporaryFile
from urllib.error import HTTPError, URLError
from urllib.request import Request, urlopen


def print_error(msg):
    sys.stderr.write(f"{msg}\n")


class VaultEnvExecError(RuntimeError):
    pass


class VaultPathVar:
    TYPE_ENV_VAR = "env_var"
    TYPE_FILE = "file"

    def __init__(self, name, mount, path, field, type_):
        self.name = name
        self.mount = mount
        self.path = path
        self.field = field
        self.type_ = type_

    @classmethod
    def from_env(cls, key: str, value: str):
        prefix = "VAULT_PATH_"
        if not key.startswith(prefix):
            return None

        name = key[len(prefix):]

        if name.endswith("_file"):
            name = name[:-5]
            type_ = VaultPathVar.TYPE_FILE
        else:
            type_ = VaultPathVar.TYPE_ENV_VAR

        try:
            path, mount = value.split("@", maxsplit=1)
            path, field = path.rsplit("/", maxsplit=1)
        except ValueError:
            print_error(f"Failed to parse value of {key} variable")
            return None

        return cls(name, mount, path, field, type_)

    def __repr__(self):
        return (f"VaultPathVar("
                f"{self.name}={self.path}@{self.mount}/{self.field},"
                f" {self.type_})")


class VaultClient:
    def __init__(self, addr, auth_role, auth_path, auth_token):
        self.addr = addr.strip("/")
        self.auth_role = auth_role
        self.auth_path = auth_path
        self.auth_token = auth_token
        self.vault_token = None

    @classmethod
    def from_env(cls):
        try:
            return cls(
                os.environ["VAULT_ADDR"],
                os.environ["VAULT_AUTH_ROLE"],
                os.environ["VAULT_AUTH_PATH"],
                os.environ["VAULT_ID_TOKEN"])
        except LookupError as e:
            raise VaultEnvExecError(
                f"Failed to configure: required variable {e}")

    def _r(self, method, url, data=None):
        json_data = None
        headers = {}
        if data:
            json_data = json.dumps(data).encode()
            headers["Content-Type"] = "application/json"
        if self.vault_token:
            headers["X-Vault-Token"] = self.vault_token

        url = f"{self.addr}{url}"
        req = Request(url, method=method, data=json_data, headers=headers)

        retries = 3
        while True:
            try:
                resp = urlopen(req)
                if resp.status not in (503, 502):
                    break
                error = f"status code: {resp.status}"
            except (HTTPError, URLError) as e:
                error = e
            if retries < 1:
                raise VaultEnvExecError(
                    f"Failed request {method} {url}: {error}")
            retries -= 1
            time.sleep(1)

        try:
            return json.load(resp)
        except JSONDecodeError as e:
            raise VaultEnvExecError(f"Failed to parse answer: {e}")

    def login(self):
        resp = self._r(
            "POST", f"/v1/auth/{self.auth_path}/login",
            data={"jwt": self.auth_token, "role": self.auth_role})
        try:
            self.vault_token = resp["auth"]["client_token"]
        except LookupError as e:
            raise VaultEnvExecError(f"No token in login answer: {e}")

    def read(self, var: VaultPathVar):
        try:
            resp = self._r("GET", f"/v1/{var.mount}/data/{var.path}")
        except HTTPError as e:
            print_error(f"Failed to get value of {var.name} variable: {e}")
            return None
        try:
            value = resp["data"]["data"][var.field]
        except LookupError:
            print_error(f"Failed to get field {var.field} "
                        f"of {var.name} variable")
            return None

        if var.type_ == VaultPathVar.TYPE_ENV_VAR:
            return value

        with NamedTemporaryFile(delete=False) as f:
            f.write(value.encode())
            return f.name


def main():
    client = VaultClient.from_env()

    client.login()

    if len(sys.argv) < 2:
        print(client.vault_token)
        return

    variables = {k: v
                 for k, v in ((var.name, client.read(var))
                              for var in (VaultPathVar.from_env(key, value)
                                          for key, value in os.environ.items())
                              if var)
                 if v}

    os.environ.update(variables)
    os.execvp(sys.argv[1], sys.argv[1:])


if __name__ == "__main__":
    try:
        main()
    except VaultEnvExecError as fatal_error:
        print_error(fatal_error)
