#!/usr/bin/env python3

import os
from subprocess import check_output
import json


def generate_providers():
    versions_info = json.loads(check_output("terraform version -json".split()))

    providers_vars = json.loads(os.environ.get("TF_VAR_providers", "{}"))
    providers = {
        provider_name: providers_vars.get(provider_name, {})
        for provider_name in (
            provider.split("/")[-1]
            for provider in versions_info["provider_selections"]
        )
    }

    if not providers:
        return

    with open("providers.tf.json", "w") as f:
        json.dump({"provider": providers}, f)


if __name__ == "__main__":
    generate_providers()
