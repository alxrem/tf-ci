#!/bin/sh

PROVIDER_VAR_RE=^TF_VAR_PROVIDERS_

export `printenv | grep $PROVIDER_VAR_RE | sed "s/$PROVIDER_VAR_RE//"`

exec terraform $@