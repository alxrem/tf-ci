FROM hashicorp/terraform:1.9.5

COPY requirements.txt /

RUN apk -U add py3-pip curl \
    && pip install --break-system-packages -r requirements.txt \
    && wget -q https://github.com/gruntwork-io/terragrunt/releases/download/v0.67.5-beta2024091201/terragrunt_linux_amd64 -O /usr/local/bin/terragrunt \
    && chmod +x /usr/local/bin/terragrunt \
    && wget -q https://github.com/terraform-linters/tflint/releases/download/v0.53.0/tflint_linux_amd64.zip -O /tmp/tflint.zip \
    && unzip -x /tmp/tflint.zip -d /bin/ \
    && rm -f /tmp/tflint.zip /var/cache/apk/*

COPY bin/* /usr/local/bin/

ENTRYPOINT ["/bin/sh"]

USER 10001